package com.codeengine.beersmanagement.controler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.codeengine.beersmanagement.entity.Beer;
import com.codeengine.beersmanagement.model.BeerVO;
import com.codeengine.beersmanagement.service.BeerService;
import com.codeengine.beersmanagement.service.CategoryService;
import com.codeengine.beersmanagement.util.BeerUtil;



/**
 * Handles requests for the application home page.
 */
@Controller
public class BeersControler {
	
	private static final Logger logger = LoggerFactory.getLogger(BeersControler.class);
	
	@Autowired
	private BeerService beerService; 
	
	@Autowired
	private CategoryService categoryService;
	public BeersControler() {
	    // pre-initialize the list of issuers available ...
		
		  
	  }
		  
	@RequestMapping(value = {  "/listBeers" })
	public String listBooks(Map<String, Object> map) {

		map.put("beer", new BeerVO());
		List<BeerVO> lstBeer = new ArrayList<BeerVO>(BeerUtil.beers.values());
		map.put("beerList", lstBeer);

		return "listBeers";
	}

	@RequestMapping("/get/{beerId}")
	public String getBeer(@PathVariable Long beerId, Map<String, Object> map) {

		//Beer beer = beerService.getBeer(beerId);
		BeerVO beer = BeerUtil.beers.get(String.valueOf(beerId));
		map.put("beer", beer);
		
		
		
		map.put("categoryList", BeerUtil.categories);
		return "beerForm";
	}
	
	
	@RequestMapping("/create")
	public String createBeer(Map<String, Object> map) {

		//Beer beer = beerService.getBeer(beerId);
		BeerVO beer = new BeerVO();
		map.put("beer", beer);
		
		
		
		map.put("categoryList", BeerUtil.categories);
		return "beerForm";
	}


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveBeer(@ModelAttribute("beer") BeerVO beer,
			BindingResult result) {

		if(null==beer.getBeerId() || "".equals(beer.getBeerId())){
			
		
			int i=0;
			for (String s : BeerUtil.beers.keySet()) {
			    if(Integer.valueOf(s) > i){
			    	i = Integer.valueOf(s);
			    }
			}
			beer.setBeerId(String.valueOf(i));
			beer.setCategoryName(BeerUtil.categories.get(beer.getCategoryId()));
		}
	
		
		BeerUtil.beers.put(beer.getBeerId(), beer);
		
		/*
		 * Note that there is no slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the current path
		 */
		return "redirect:/listBeers";
	}

	@RequestMapping("/delete/{beerId}")
	public String deleteBeer(@PathVariable("beerId") Long id) {

		//beerService.deleteBeer(id);
		BeerUtil.beers.remove(String.valueOf(id));
		/*
		 * redirects to the path relative to the current path
		 */
		// return "redirect:../listBooks";

		/*
		 * Note that there is the slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the project root path
		 */
		return "redirect:/listBeers";
	}
	
	@RequestMapping("/seachByCategory")
	public String seachByCategory(@RequestParam(value = "term", required = false)  String term,Map<String, Object> map) {

		List<BeerVO> lstBeer = new ArrayList<BeerVO>();
		List<BeerVO> lst = new ArrayList<BeerVO>(BeerUtil.beers.values());
		for (BeerVO beerVO : lst) {
			if(beerVO.getCategoryId().equals(term)){
				lstBeer.add(beerVO);
			}
		}
		map.put("beerList", lstBeer);
		/*
		 * redirects to the path relative to the current path
		 */
		// return "redirect:../listBooks";

		/*
		 * Note that there is the slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the project root path
		 */
		
		return "listBeersAjax";
	}
	
}
