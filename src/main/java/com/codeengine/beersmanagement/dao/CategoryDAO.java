/**
 * 
 */
package com.codeengine.beersmanagement.dao;

import java.util.List;

import com.codeengine.beersmanagement.entity.Category;

/**
 * @author TrungPham
 *
 */
public interface CategoryDAO {
	/*
	 * CREATE and UPDATE
	 */
	public void saveCategory(Category category); // create and update

	/*
	 * READ
	 */
	public List<Category> listCategorys();
	public Category getCategory(Long id);

	/*
	 * DELETE
	 */
	public void deleteCategory(Long id);
}
