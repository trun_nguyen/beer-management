/**
 * 
 */
package com.codeengine.beersmanagement.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author TrungPham
 *
 */
public class ListBeerWrapperVO implements Serializable{
	private int draw;
	private int recordsTotal;
	private int recordsFiltered;
	private List<BeerVO> data;
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public List<BeerVO> getData() {
		return data;
	}
	public void setData(List<BeerVO> data) {
		this.data = data;
	}
	
	
}
