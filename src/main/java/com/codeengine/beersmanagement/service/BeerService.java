/**
 * 
 */
package com.codeengine.beersmanagement.service;

import java.util.List;

import com.codeengine.beersmanagement.entity.Beer;

/**
 * @author TrungPham
 *
 */
public interface BeerService {
	/*
	 * CREATE and UPDATE
	 */
	public void saveBeer(Beer beer); // create and update

	/*
	 * READ
	 */
	public List<Beer> listBeers();
	public Beer getBeer(Long id);

	/*
	 * DELETE
	 */
	public void deleteBeer(Long id);
}
