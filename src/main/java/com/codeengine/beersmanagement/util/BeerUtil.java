/**
 * 
 */
package com.codeengine.beersmanagement.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.codeengine.beersmanagement.model.BeerVO;

/**
 * @author TrungPham
 *
 */
public class BeerUtil {
	public static  Map<String, BeerVO> beers = new HashMap<String, BeerVO>();
	public static  Map<String,String> categories = new LinkedHashMap<String,String>();
	static{
		categories.put("01", "United Stated");
		categories.put("02", "China");
		categories.put("03", "Singapore");
		categories.put("04", "Malaysia");
		categories.put("05", "United Kingdom");
		BeerVO beerVO = null;
	     for(int i=0; i < 50;i++){
	    	 if(i < 10){
	    	 beerVO = new BeerVO("Sai Gon Factory "+i, "Sai Gon Export "+i, "01", "USA",3.25,"should use",false,String.valueOf(i));
	    	 }else if(i < 20){
	    		 beerVO = new BeerVO("Hue Factory "+i, "Hue Export "+i, "02", "USA",3.25,"should use",false,String.valueOf(i));
	    	 }else if(i < 30){
	    		 beerVO = new BeerVO("Da Nang Factory "+i, "Lague Export "+i, "03", "USA",3.25,"should use",false,String.valueOf(i));
	    	 }else if(i < 40){
	    		 beerVO = new BeerVO("Ha Noi Factory "+i, "Ha Noi Export "+i, "04", "USA",3.25,"should use",false,String.valueOf(i));
	    	 }else{
	    		 beerVO = new BeerVO("Can Tho Factory "+i, "Can Tho Export "+i, "05", "USA",3.25,"should use",false,String.valueOf(i));
	    	 }
	    	 beerVO.setCategoryName(categories.get(beerVO.getCategoryId()));
	    	 beers.put(String.valueOf(i), beerVO);
	     }
	     
			
	}
	
}
