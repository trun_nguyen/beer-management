<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
	.custom-combobox {
		position: relative;
		display: inline-block;
	}
	.custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
		/* support: IE7 */
		*height: 1.7em;
		*top: 0.1em;
	}
	.custom-combobox-input {
		margin: 0;
		padding: 0.3em;
	}
	</style>

<c:url var="actionUrl" value="save" />



<form:form id="beerForm" commandName="beer" method="post"
	action="${actionUrl }" class="pure-form pure-form-aligned">

	<fieldset>
		<legend></legend>
		<div class="pure-control-group">
			<label for="manufacturer">Manufacturer</label>
			<form:input id="manufacturerId" path="manufacturer"  name="manufacturer"/>			
		</div>
		<div class="pure-control-group">
			<label for="name">Name</label>
			<form:input name="name" id = "nameId" path="name" placeholder="name"/>
		</div>
		<div class="ui-widget">
		
			<label for="categoryId">Category</label>
			<form:select path="categoryId" id="categoryId">
					  <form:options items="${categoryList}" />
			</form:select>
<%-- 			<form:select path="categoryId" items="${country}"/> --%>
<%-- 			<form:input path="categoryId" placeholder="categoryId" name="categoryId"/> --%>

		</div>
		
		<div class="pure-control-group">
			<label for="country">Country</label>
			<form:input path="country" placeholder="country" />
		</div>
		<div class="pure-control-group">
			<label for="price">Price</label>
			<form:input path="price" placeholder="price" />
		</div>
		<div class="pure-control-group">
			<label for="description">Description</label>
			<form:input path="description" placeholder="description" />
		</div>
		<div class="pure-control-group">
			<label for="achiveFlg">Archive</label>
			<form:checkbox path="achiveFlg" />
		</div>

		<form:input path="beerId" type="hidden" />

	</fieldset>
</form:form>
<script type="text/javascript"
		src='<c:url value="/resources/js/js-for-Beers.js"/>'></script>
